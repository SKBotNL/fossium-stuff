﻿using HtmlAgilityPack;

var client = new HttpClient();

HtmlWeb web = new HtmlWeb();
HtmlDocument doc = web.Load("https://nuget.emzi0767.com/gallery/package/DSharpPlus/");
string latestversion = doc.DocumentNode.SelectSingleNode("//body/div[@class='slimget-content']/div[@class='slimget-content-inner']/div[@class='details-outer']/div[@class='details-inner-left']/div[@class='details-header']/div[@class='details-id']/div[@class='details-id-version']").InnerText;

try
{
    using (var stream = await client.GetStreamAsync($"https://nuget.emzi0767.com/api/v3/flatcontainer/dsharpplus/{latestversion}/dsharpplus.{latestversion}.nupkg"))
    {
        using (var fileStream = new FileStream("/home/runner/work/download/dsharpplus.nupkg", FileMode.CreateNew))
        {
            await stream.CopyToAsync(fileStream);
        }
    }
    using (var stream = await client.GetStreamAsync($"https://nuget.emzi0767.com/api/v3/flatcontainer/dsharpplus.interactivity/{latestversion}/dsharpplus.interactivity.{latestversion}.nupkg"))
    {
        using (var fileStream = new FileStream("/home/runner/work/download/dsharpplus.interactivity.nupkg", FileMode.CreateNew))
        {
            await stream.CopyToAsync(fileStream);
        }
    }
    using (var stream = await client.GetStreamAsync($"https://nuget.emzi0767.com/api/v3/flatcontainer/dsharpplus.lavalink/{latestversion}/dsharpplus.lavalink.{latestversion}.nupkg"))
    {
        using (var fileStream = new FileStream("/home/runner/work/download/dsharpplus.lavalink.nupkg", FileMode.CreateNew))
        {
            await stream.CopyToAsync(fileStream);
        }
    }
    using (var stream = await client.GetStreamAsync($"https://nuget.emzi0767.com/api/v3/flatcontainer/dsharpplus.slashcommands/{latestversion}/dsharpplus.slashcommands.{latestversion}.nupkg"))
    {
        using (var fileStream = new FileStream("/home/runner/work/download/dsharpplus.slashcommands.nupkg", FileMode.CreateNew))
        {
            await stream.CopyToAsync(fileStream);
        }
    }
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
    Environment.Exit(1);
}